﻿using _8086Sim.ConsoleApp.Instructions;
using FluentResults;

namespace _8086Sim.ConsoleApp;

internal static class InstructionParser
{
    const int DestinationBitMask = 0b00111000;
    const int SourceBitMask = 0b00000111;

    internal static Result<IReadOnlyCollection<Instruction>> ParseByteCode(byte[] bytes)
    {
        var instructions = new List<Instruction>();

        for (int i = 0; i < bytes.Length; i += 2)
        {
            if (bytes[i] >> 2 != Instruction.MovOperationCode)
            {
                return Result.Fail($"Only a MOV operation is supported. Found: {Convert.ToString(bytes[i], 2)}");
            }

            var w = bytes[i].IsBitSet(0);
            var d = bytes[i].IsBitSet(1);

            // expect two top bits to be set
            if ((bytes[i + 1] & 0b11000000) != 0b11000000)
            {
                return Result.Fail("Expected mod to be set but it wasn't");
            }

            var destinationBits = (bytes[i + 1] & DestinationBitMask) >> 3;
            var sourceBits = bytes[i + 1] & SourceBitMask;

            if (w)
            {
                destinationBits.SetBit(3, true);
                sourceBits.SetBit(3, true);
            }

            var destination = (Register)destinationBits;
            var source = (Register)sourceBits;

            if (!d)
            {
                var temp = destination;
                destination = source;
                source = temp;
            }

            instructions.Add(new MoveInstruction(destination, source));
        }

        return instructions.AsReadOnly();
    }
}
