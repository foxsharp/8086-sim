﻿namespace _8086Sim.ConsoleApp;

internal static class ByteExtensions
{
    internal static bool IsBitSet(this byte b, int pos) => (b & (1 << pos)) != 0;

    internal static void SetBit(this ref int b, int pos, bool value) => b = value ? (byte)(b | (1 << pos)) : (byte)(b & ~(1 << pos));
}
