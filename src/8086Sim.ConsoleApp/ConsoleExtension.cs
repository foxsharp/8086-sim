﻿namespace _8086Sim.ConsoleApp;

internal static class ConsoleExtension
{
    internal static void WriteInColor(string message, ConsoleColor foreground = ConsoleColor.White)
    {
        Console.ForegroundColor = foreground;
        Console.Write(message);
        Console.ResetColor();
    }
}
