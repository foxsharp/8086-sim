﻿using _8086Sim.ConsoleApp;
using _8086Sim.ConsoleApp.Instructions;
using _8086Sim.ConsoleApp.Verification;
using System.Text;
using static _8086Sim.ConsoleApp.ConsoleExtension;

if (!args.Any())
{
    Console.WriteLine("Usage: 8086sim.exe .asm file");
    return;
}

WriteInColor("Assembling input file...\n", ConsoleColor.Yellow);

var inputAssemblyFile = args[0];

var inputAssemblyResult = Nasm.AssembleFile(inputAssemblyFile);
if (!inputAssemblyResult.IsSuccess)
{
    WriteInColor("Assembling the input file failed:\n", ConsoleColor.Red);
    WriteInColor(string.Join("\n", inputAssemblyResult.Errors), ConsoleColor.Red);
    return;
}

var assembledInputFile = inputAssemblyResult.Value;
WriteInColor($"Input file assembled: {assembledInputFile}\n", ConsoleColor.Green);

byte[] bytes;
try
{
    bytes = File.ReadAllBytes(assembledInputFile);
}
catch (Exception e)
{
    Console.WriteLine($"Could not read file '{Path.GetFileName(args[0])}': {e}");
    return;
}

WriteInColor("Pasing assembled file...\n", ConsoleColor.Yellow);

var instructionsResult = InstructionParser.ParseByteCode(bytes);
if (!instructionsResult.IsSuccess)
{
    WriteInColor("Parsing assembled file failed:\n", ConsoleColor.Red);
    WriteInColor(string.Join("\n", instructionsResult.Errors), ConsoleColor.Red);
    return;
}

var instructions = instructionsResult.Value;

WriteInColor("Parsing successful\n", ConsoleColor.Green);
WriteInColor("Generating assembly from parsed instructions...\n", ConsoleColor.Yellow);

var sb = new StringBuilder();
sb.AppendLine("bits 16");

foreach(var instruction in instructions)
{
    var i = instruction as MoveInstruction;

    if (i is null)
    {
        throw new NotImplementedException("An unexpected instruction was parsed.");
    }

    WriteInColor(i.Name, ConsoleColor.Cyan);
    WriteInColor(" ");
    WriteInColor(i.Destination.ToString(), ConsoleColor.Green);
    WriteInColor(", ");
    WriteInColor(i.Source.ToString(), ConsoleColor.Green);
    WriteInColor("\n");

    sb.AppendLine($"{i.Name} {i.Destination}, {i.Source}");
}

var outputFile = Temp.GetFilePathWithExtension("asm");
File.WriteAllText(outputFile, sb.ToString());

WriteInColor($"Assembly generated successfully: {outputFile}\n", ConsoleColor.Green);
WriteInColor("Reassembling output file...\n", ConsoleColor.Yellow);

var reassembledOutputResult = Nasm.AssembleFile(outputFile);
if (!reassembledOutputResult.IsSuccess)
{
    WriteInColor("Reassembling output failed:\n", ConsoleColor.Red);
    WriteInColor(string.Join("\n", reassembledOutputResult.Errors), ConsoleColor.Red);
    return;
}

var reassembledOutputFile = reassembledOutputResult.Value;

WriteInColor($"Reassembly successful: {reassembledOutputFile}\n", ConsoleColor.Green);

WriteInColor("Comparing the two assemblies...\n", ConsoleColor.Yellow);

var comparisonResult = FileCompare.FilesMatch(assembledInputFile, reassembledOutputFile);

if (!comparisonResult.IsSuccess)
{
    WriteInColor("[FAIL] - The Parser output does not match the original assembly:\n", ConsoleColor.Red);
    WriteInColor(string.Join("\n", comparisonResult.Errors), ConsoleColor.Red);
    return;
}

WriteInColor("[SUCCESS] - The Parser output matches the original assembly!\n", ConsoleColor.Green);
