﻿namespace _8086Sim.ConsoleApp.Instructions;

internal record MoveInstruction : Instruction
{
    internal override string Name => "MOV";

    internal override int OpCode => MovOperationCode;

    internal Register Destination { get; private set; }

    internal Register Source { get; private set; }

    public MoveInstruction(Register destination, Register source)
    {
        Destination = destination;
        Source = source;
    }
}
