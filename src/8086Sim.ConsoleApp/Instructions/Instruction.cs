﻿namespace _8086Sim.ConsoleApp.Instructions;

internal abstract record Instruction
{
    internal const byte MovOperationCode = 0b100010;

    internal abstract string Name { get; }

    internal abstract int OpCode { get; }
}
