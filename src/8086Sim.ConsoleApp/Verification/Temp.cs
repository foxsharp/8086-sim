﻿namespace _8086Sim.ConsoleApp.Verification;

internal static class Temp
{
    internal static string GetFilePath() => Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

    /// <summary>
    /// Generates a new random file in the temp directory with a specified extension
    /// </summary>
    /// <param name="extension">Extension with or without the leading period</param>
    /// <returns>A full path the the temp file</returns>
    internal static string GetFilePathWithExtension(string extension) => Path.ChangeExtension(Path.Combine(Path.GetTempPath(), Path.GetRandomFileName()), extension);
}
