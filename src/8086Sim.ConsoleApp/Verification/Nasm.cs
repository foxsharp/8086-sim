﻿using FluentResults;
using System.Diagnostics;

namespace _8086Sim.ConsoleApp.Verification;

internal static class Nasm
{
    /// <summary>
    /// Assembles an assembly code file
    /// </summary>
    /// <param name="file">Input assembly file</param>
    /// <returns>Result with a path to the assembled temp file or an error output</returns>
    internal static Result<string> AssembleFile(string file)
    {
        var outputFile = Temp.GetFilePath();

        Process process = new()
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "nasm.exe",
                Arguments = $"{file} -o {outputFile}",
                WindowStyle = ProcessWindowStyle.Hidden,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            }
        };

        process.Start();

        _ = process.StandardOutput.ReadToEnd();
        var error = process.StandardError.ReadToEnd();

        process.WaitForExit();

        if (process.ExitCode == 0)
        {
            return Result.Ok(outputFile);
        }
        else
        {
            return Result.Fail<string>(error);
        }
    }
}
