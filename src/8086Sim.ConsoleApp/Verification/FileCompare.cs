﻿using FluentResults;
using System.Diagnostics;

namespace _8086Sim.ConsoleApp.Verification;

internal static class FileCompare
{
    /// <summary>
    /// Compares two files using fc.exe
    /// </summary>
    /// <param name="fileA">First full file path</param>
    /// <param name="fileB">Second full file path</param>
    /// <returns>Result object of OK if the files match, or an Error with fc.exe output as string</returns>
    internal static Result FilesMatch(string fileA, string fileB)
    {
        Process process = new();
        process.StartInfo.FileName = "fc.exe";
        var args = $"\"{fileA}\" \"{fileB}\"";

        process.StartInfo.Arguments = args;
        process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
        process.StartInfo.UseShellExecute = false;
        process.StartInfo.RedirectStandardOutput = true;

        process.Start();

        string output = process.StandardOutput.ReadToEnd();
        process.WaitForExit();

        // "FC: no differences encountered" is output when files are the same
        if (output.Contains("FC: no differences encountered"))
        {
            return Result.Ok();
        }

        return Result.Fail($"Files differ:\n{output}");
    }
}
